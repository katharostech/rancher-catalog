# Maestro TLS #

**Note:** This stack is experimental and is only meant to be used for the following scenario.

Maestro will allow the easy setting up of routes to your Rancher services with automated, trusted HTTPS cert generation and renewal.

Here is how the environment should be configured to use maestro:

1. The DNS for all websites that are to be hosted should be pointed to the entire Rancher environment cluster. The Maestro TLS load balancer will capture all traffic to any of the hosts in the cluster.
2. There should be no other services that bind the ports `80` and `443` in the whole Rancher environment
3. You deploy the Maestro TLS stack. ( Note: The `example-agent` service that gets created may need to be stopped manually. It doesn't need to be running, it is only there for reference )
4. Clone the `example-agent` service to a new service, one for every website that you want to rout traffic too. Before saving the cloned service, update the environment variables with the appropriate values for your website and Rancher environment.
