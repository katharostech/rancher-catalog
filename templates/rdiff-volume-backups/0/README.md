# Rancher NFS backups

This container will backup all of your Rancher NFS volumes to another NFS device using rdiff incremental backups. See the [container documentation](https://github.com/kadimasolutions/docker_rdiff-volume-backup) for details.
