# Phabricator

Phabricator is a tool for software development and collaboration. It hosts a surplus of tools that you can use to do your work more efficiently. It includes hosted git, a wiki, and issue management and taskboards.
